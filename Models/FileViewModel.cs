﻿using System;
using Microsoft.AspNetCore.Http;

namespace forum_lab6.Models
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public IFormFile FilePath { get; set; }
    }
}
