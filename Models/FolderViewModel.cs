﻿using System;

namespace forum_lab6.Models
{
    public class FolderViewModel
    {
        public string Name { get; set; }
        public Guid? FolderId { get; set; }
    }
}
